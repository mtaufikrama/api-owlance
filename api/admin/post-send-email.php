<?php
include "../token/cek-token-admin.php";

// id, send_email_name, subject, body, id_email

if (!empty($send_email_name)) $dataSendEmail['send_email_name'] = $send_email_name;
if (!empty($nama)) $dataSendEmail['nama'] = $nama;
if (!empty($subject)) $dataSendEmail['subject'] = $subject;
if (!empty($body)) $dataSendEmail['body'] = $body;
if (!empty($id_email)) $dataSendEmail['id_email'] = $id_email;

if (!empty($id)) {
	$cekID = baca_tabel('send_email', 'count(*)', "where binary id = '$id'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID tidak ada";
		echo encryptData($datax);
		die();
	}
}

if (empty($id)) {
	$cekNama = baca_tabel('send_email', 'count(*)', "where subject='$subject' or body='$body'");

	if ($cekNama > 0) {
		$datax['code'] = 404;
		$datax['msg'] = "Subject $subject atau Body $body sudah tersedia";
		echo encryptData($datax);
		die();
	}
	$action = 'create';
	$id = generateID(15, 'send_email', 'id');
	$dataSendEmail['id'] = $id;
	$result = insert_tabel('send_email', $dataSendEmail);
} else {
	$action = 'update';
	$result = update_tabel('send_email', $dataSendEmail, "where binary id='$id'");
}

if ($result) {
	activity_user($id_user, 'post-send-email', json_encode($dataSendEmail), $action);
	$datax['code'] = 200;
	$datax['msg'] = 'Berhasil';
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal";
}
echo encryptData($datax);
