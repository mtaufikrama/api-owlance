<?php
include "../token/cek-token-admin.php";

// id, nama

if (!empty($nama)) $dataKategori['nama'] = $nama;

if (!empty($id)) {
	$cekID = baca_tabel('kategori', 'count(*)', "where binary id = '$id'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID tidak ada";
		echo encryptData($datax);
		die();
	}
}

if (empty($id)) {
	$cekNama = baca_tabel('kategori', 'count(*)', "where nama='$nama'");

	if ($cekNama > 0) {
		$datax['code'] = 404;
		$datax['msg'] = "kategori $nama sudah tersedia";
		echo encryptData($datax);
		die();
	}
	$action = 'create';
	$id = generateID(15, 'kategori', 'id');
	$dataKategori['id'] = $id;
	$result = insert_tabel('kategori', $dataKategori);
} else {
	$action = 'update';
	$result = update_tabel('kategori', $dataKategori, "where binary id='$id'");
}

if ($result) {
	activity_user($id_user, 'post-kategori', json_encode($dataKategori), $action);
	$datax['code'] = 200;
	$datax['msg'] = 'Berhasil';
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal";
}

echo encryptData($datax);
