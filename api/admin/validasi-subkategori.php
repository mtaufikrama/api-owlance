<?php
include "../token/cek-token-admin.php";

// id

if (empty($id)) {
	$datax['code'] = 404;
	$datax['msg'] = "ID tidak ada";
	echo encryptData($datax);
	die();
}

$cekValidasi = baca_tabel('subkategori', 'is_validate', "where binary='$id'");

if ($cekValidasi != 1) {
	$action = 'delete';
	$validasi['is_validate'] = 0;
} else {
	$action = 'update';
	$validasi['is_validate'] = 1;
}

$result = update_tabel('subkategori', $validasi, "where binary id='$id'");

if ($result) {
	activity_user($id_user, 'validasi-subkategori', '{"id":"' . $id . '"}', $action);
	$datax['code'] = 200;
	$datax['msg'] = "Berhasil $action";
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal";
}
echo encryptData($datax);
