<?php
include "../token/cek-token-admin.php";

// id, nama, inisial, kode_telepon, kode_bahasa

if (!empty($nama)) $dataNegara['nama_negara'] = $nama;
if (!empty($inisial)) $dataNegara['inisial_negara'] = $inisial;
if (!empty($kode_telepon)) $dataNegara['kode_telepon'] = $kode_telepon;
if (!empty($kode_bahasa)) $dataNegara['kode_bahasa'] = $kode_bahasa;

if (!empty($id)) {
	$cekID = baca_tabel('negara', 'count(*)', "where binary id_negara = '$id'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID tidak ada";
		echo encryptData($datax);
		die();
	}
}

if (empty($id)) {
	$cekNama = baca_tabel('negara', 'count(*)', "where nama_negara='$nama'");

	if ($cekNama > 0) {
		$datax['code'] = 404;
		$datax['msg'] = "Nama Negara $nama sudah tersedia";
		echo encryptData($datax);
		die();
	}
	$action = 'create';
	$result = insert_tabel('negara', $dataNegara);
} else {
	$action = 'update';
	$result = update_tabel('negara', $dataNegara, "where binary id_negara='$id'");
}

if ($result) {
	activity_user($id_user, 'post-negara', json_encode($dataNegara), $action);
	$datax['code'] = 200;
	$datax['msg'] = 'Berhasil';
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal";
}
echo encryptData($datax);
