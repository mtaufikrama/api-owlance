<?php
include "../token/cek-token-admin.php";

// id, id_kritik_saran, balasan

if (empty($balasan)) {
	$datax['code'] = 500;
	$datax['msg'] = "Title tidak ada";
	echo encryptData($datax);
	die();
}

if (!empty($id)) {
	$cekID = baca_tabel('kritik_saran', 'count(*)', "where binary id = '$id'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID tidak ada";
		echo encryptData($datax);
		die();
	}
}

$dataKritikSaran['id_kritik_saran'] = $id_kritik_saran;
$dataKritikSaran['id_user'] = $id_user;
$dataKritikSaran['caption'] = $balasan;
$dataKritikSaran['waktu'] = date("Y-m-d H:i:s");

if (empty($id)) {
	$action = 'create';
	$id = generateID(50, 'kritik_saran', 'id');
	$dataKritikSaran['id'] = $id;
	$result = insert_tabel('kritik_saran', $dataKritikSaran);
} else {
	$action = 'update';
	$result = update_tabel('kritik_saran', $dataKritikSaran, "where binary id='$id'");
}

if ($result) {
	activity_user($id_user, 'balas-kritik-saran', json_encode($dataKritikSaran), $action);
	$datax['code'] = 200;
	$datax['msg'] = "Berhasil $action Balasan Kritik Saran";
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal $action Balasan Kritik Saran";
}
echo encryptData($datax);
