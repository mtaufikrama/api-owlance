<?php
include "../token/cek-token-admin.php";

//array(files), tabs, kode

$id_tabs = baca_tabel("tabs", "id", "where nama='$tabs'");

if (!$id_tabs || $id_tabs == '') {
    $datax['code'] = 404;
    $datax['msg'] = "Data Tidak Ditemukan";
    echo encryptData($datax);
    die();
}

if (!is_array($_FILES)) {
    $datax['code'] = 500;
    $datax['msg'] = 'Tidak ada foto yang diinput';
    echo encryptData($datax);
    die();
}

$size = 0;

foreach ($_FILES as $key => $element) {
    $size = $size + $element['size'];
    if ($size >= 2000000) {
        $datax['code'] = 500;
        $datax['msg'] = 'Data Terlalu Besar';
        echo encryptData($datax);
        die();
    }
    $images[] = $element;
}

$result = true;

foreach ($images as $image) {
    $dataImages['id'] = generateID(250, 'images', 'id');
    $dataImages['id_user'] = $id_user;
    $dataImages['mime_type'] = $image['type'];
    $dataImages['image'] = base64_encode(file_get_contents($image['tmp_name']));
    $dataImages['waktu'] = date("Y-m-d H:i:s");
    if ($result) $result = insert_tabel('images', $dataImages);

    $userImg['id'] = generateID(100, 'tabs_img', 'id');
    $userImg['id_images'] = $dataImages['id'];
    $userImg['id_tabs'] = $id_tabs;
    $userImg['kode'] = $kode;
    $userImg['waktu'] = date("Y-m-d H:i:s");
    $userImg['action'] = 0;
    if ($result) $result = insert_tabel('tabs_img', $userImg);
}

if ($result) {
    $datax['code'] = 200;
    $datax['msg'] = 'Foto Berhasil diupload';
} else {
    $datax['code'] = 500;
    $datax['msg'] = 'Foto Gagal diupload';
}

echo encryptData($datax);
