<?php
include "../token/cek-token-admin.php";

// id, email, password

if (!empty($email)) $dataEmail['email'] = $email;
if (!empty($password)) $dataEmail['password'] = base64_encode(enkrip($password));

if (!empty($id)) {
	$cekID = baca_tabel('email', 'count(*)', "where binary id = '$id'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID tidak ada";
		echo encryptData($datax);
		die();
	}
}

if (empty($id)) {
	$cekNama = baca_tabel('email', 'count(*)', "where email='$email'");

	if ($cekNama > 0) {
		$datax['code'] = 404;
		$datax['msg'] = "Email $email sudah tersedia";
		echo encryptData($datax);
		die();
	}
	$action = 'create';
	$id = generateID(15, 'email', 'id');
	$dataEmail['id'] = $id;
	$result = insert_tabel('email', $dataEmail);
} else {
	$action = 'update';
	$result = update_tabel('email', $dataEmail, "where binary id='$id'");
}

if ($result) {
	activity_user($id_user, 'post-email', json_encode($dataEmail), $action);
	$datax['code'] = 200;
	$datax['msg'] = 'Berhasil';
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal";
}
echo encryptData($datax);
