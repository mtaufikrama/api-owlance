<?php
include "../token/cek-token-admin.php";

$sql = "SELECT a.nama, a.email, a.username, b.nama_roles, a.id_user_img,
c.nama_provinsi as provinsi, d.nama_kota as kota, 
e.nama_kecamatan as kecamatan, f.nama_kelurahan as kelurahan
FROM user a 
LEFT JOIN roles b ON a.id_roles = b.id
LEFT JOIN provinsi c ON a.id_provinsi = c.id_provinsi
LEFT JOIN kota d ON a.id_kota = d.id_kota
LEFT JOIN kecamatan e ON a.id_kecamatan = e.id_kecamatan
LEFT JOIN kelurahan f ON a.id_kelurahan = f.id_kelurahan
WHERE a.action<>2
ORDER BY b.nama_roles, a.waktu DESC";

$run = $db->Execute($sql);

while ($get = $run->fetchRow()) {
	$idfoto = baca_tabel('user_img', 'id_images', "where binary id='" . $get['id_user_img'] . "'");
	$get['foto'] = image_link($idfoto);
	unset($get['id_user_img']);
	$users[] = $get;
}

if (is_array($users)) {
	$datax['code'] = 200;
	$datax['msg'] = 'Berhasil';
	$datax['users'] = $users;
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Data Tidak Ditemukan";
}
echo encryptData($datax);
