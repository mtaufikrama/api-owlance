<?php
include "../token/cek-token.php";

//id_kecamatan

if (empty($id_kecamatan)) {
	$datax['code'] = 404;
	$datax['msg'] = "ID Kecamatan tidak ada";
	echo encryptData($datax);
	die();
}

$sql = "SELECT id_kelurahan as kode, nama_kelurahan as nama, kode_pos from kelurahan where id_kecamatan='$id_kecamatan'";

$run = $db->Execute($sql);

while ($get = $run->fetchRow()) {
	$data[] = $get;
}

if (is_array($data)) {
	$datax['code'] = 200;
	$datax['kelurahan'] = $data;
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Tidak ada data ditemukan";
}
echo encryptData($datax);
