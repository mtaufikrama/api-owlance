<?php
include "../token/cek-token.php";

//id_kota

if (empty($id_kota)) {
	$datax['code'] = 404;
	$datax['msg'] = "ID Kota tidak ada";
	echo json_encode($datax);
	die();
}

$sql = "SELECT id_kecamatan as kode, nama_kecamatan as nama from kecamatan where id_kota='$id_kota'";

$run = $db->Execute($sql);

while ($get = $run->fetchRow()) {
	$data[] = $get;
}

if (is_array($data)) {
	$datax['code'] = 200;
	$datax['list'] = $data;
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Tidak ada data ditemukan";
}
echo encryptData($datax);
