<?php
include "../token/cek-no-token.php";

// username

$cek = baca_tabel('user', 'count(username)', "where binary username='$username'");

if (empty($username) || $cek <= 0) {
	$datax['code'] = 404;
	$datax['msg'] = "Data Tidak Ditemukan";
	echo encryptData($datax);
	die();
}

$sql = "SELECT a.id, a.nama, a.email, b.nama_roles, 
a.id_user_img as foto,
c.nama_provinsi as provinsi, d.nama_kota as kota, 
e.nama_kecamatan as kecamatan, f.nama_kelurahan as kelurahan
from user a 
left join roles b on a.id_roles=b.id
left join provinsi c on a.id_provinsi=c.id_provinsi
left join kota d on a.id_kota=d.id_kota
left join kecamatan e on a.id_kecamatan=e.id_kecamatan
left join kelurahan f on a.id_kelurahan=f.id_kelurahan
where binary a.username = '$username'";

$run = $db->Execute($sql);

while ($get = $run->fetchRow()) {
	$id_users = $get['id'];
	$get['followers'] = baca_tabel('followers', 'count(*)', "where binary id_user_lawan='$id_users' and is_follow='1'");
	$get['following'] = baca_tabel('followers', 'count(*)', "where binary id_user='$id_users' and is_follow='1'");
	$get['is_follow'] = false;
	if (!empty($id_user)) {
		$is_follow = baca_tabel('followers', 'count(*)', "where binary id_user='$id_user' 
			and binary id_user_lawan='$id_users' 
			and is_follow='1'");
		if ($is_follow > 0) {
			$get['is_follow'] = true;
		}
	}
	$idfoto = baca_tabel('user_img', 'id_images', "where binary id='" . $get['foto'] . "'");
	$get['foto'] = image_link($idfoto);
	unset($get['id']);
	$isEdit = false;
	if ($id_users == $id_user) $isEdit = true;
	$get['is_edit'] = $isEdit;

	$user = $get;
}

if (is_array($user)) {
	$datax['code'] = 200;
	$datax['data'] = $user;
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Tidak ada data ditemukan";
}
echo encryptData($datax);
