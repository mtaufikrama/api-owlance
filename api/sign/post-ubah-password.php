<?php
include "../token/cek-no-token.php";

// otp / pw_lama, pw_baru, device_data, alamat, gmt

$md_pw_baru = base64_encode(enkrip($pw_baru));

if (!empty($otp) && !empty($pw_lama)) {
	$datax['code'] = 500;
	$datax['msg'] = 'Pilih salah satu';
	echo encryptData($datax);
	die();
}

if (empty($otp) && empty($pw_lama)) {
	$datax['code'] = 500;
	$datax['msg'] = 'Harus diisi semua';
	echo encryptData($datax);
	die();
}

if (!empty($otp)) {

	$email = baca_tabel('otp', 'email', "where otp='$otp'");

	if (!empty($email)) {

		delete_tabel('otp', "where email='$email'");

		$data['password'] = $md_pw_baru;
		$result = update_tabel('user', $data, "where email='$email'");

		if ($result) {
			$id_user = baca_tabel('user', 'id', "where email='$email' and password='$md_pw_baru'");
			$result = delete_tabel('login', "where binary id_user='$id_user'");

			$gmtName = " GMT+$gmt";
			if ($gmt < 0) $gmtName = " GMT$gmt";
			$params["device_name"] = device_name($device_data);
			$params['waktu_perubahan'] = date_time(date("Y-m-d H:i:s"), $token) . $gmtName;
			$params['lokasi'] = location_name();
			$params['nama_pengguna'] = baca_tabel('user', 'nama', "where binary id = '$id_user'");

			$datax['code'] = 200;
			$datax['msg'] = 'Password berhasil diubah';
			$datax['pw'] = $pw_baru;
		} else {
			$datax['code'] = 500;
			$datax['msg'] = "Maaf, Password Gagal diubah";
		}
	} else {
		$datax['code'] = 500;
		$datax['msg'] = "OTP Tidak Ditemukan";
	}
} else {

	if (!empty($id_user)) {

		$pw_lama = base64_encode(enkrip($pw_lama));

		if ($md_pw_baru == $pw_lama) {
			$datax['code'] = 500;
			$datax['msg'] = 'Password Lama dan Password Baru Tidak Boleh Sama';
			echo encryptData($datax);
			die();
		}

		$pw_lama_real = baca_tabel('user', 'password', "where binary id='$id_user'");

		$email = baca_tabel('user', 'email', "where binary id='$id_user'");

		if ($pw_lama_real == $pw_lama) {
			$data['password'] = $md_pw_baru;
			$result = update_tabel('user', $data, "where binary id='$id_user'");
			if ($result) {
				$result = delete_tabel('login', "where binary id_user='$id_user' and token<>$authorized");

				$gmtName = " GMT+$gmt";
				if ($gmt < 0) $gmtName = " GMT$gmt";
				$params["device_name"] = device_name($device_data);
				$params['waktu_perubahan'] = date_time(date("Y-m-d H:i:s"), $token) . $gmtName;
				$params['lokasi'] = location_name();
				$params['nama_pengguna'] = baca_tabel('user', 'nama', "where binary id = '$id_user'");

				$datax['code'] = 200;
				$datax['msg'] = 'Password berhasil diubah';
				$datax['pw'] = $pw_baru;
			} else {
				$datax['code'] = 500;
				$datax['msg'] = "Maaf, Password Gagal diubah";
			}
		} else {
			$datax['code'] = 500;
			$datax['msg'] = 'Password Lama Tidak Sesuai';
		}
	} else {
		$datax['code'] = 300;
		$datax['msg'] = 'Akses Ditolak';
	}
}

echo encryptData($datax);

if ($datax['code'] == 200) send_email('ubah-pass', $email, $params);
