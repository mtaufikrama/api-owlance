<?php
include "../token/cek-no-token.php";

// otp / email

$waktu_sekarang = date("Y-m-d H:i:s");

$durasi = baca_tabel('otp', 'durasi', "where (email='$email' or otp='$otp') order by waktu desc limit 1");

if (empty($durasi) || $durasi <= 0) {
	$datax['code'] = 404;
	$datax['msg'] = 'Tidak ada permintaan OTP';
	echo encryptData($datax);
	die();
}

$waktu_lalu = date("Y-m-d H:i:s", strtotime($waktu_sekarang . " - $durasi minutes"));

$cekWaktu = baca_tabel('otp', 'count(*)', "where (email='$email' or otp='$otp') and waktu >= '$waktu_lalu' order by waktu desc limit 1");

if ($cekWaktu <= 0) {
	$datax['code'] = 404;
	$datax['msg'] = "Waktu sudah lebih dari $durasi menit, Silahkan Kirim Ulang OTP Kembali";
	echo encryptData($datax);
	die();
}

// delete_tabel('otp', "where (email='$email' or binary id='$otp') or waktu >= '$waktu_lalu'");

$result = baca_tabel('otp', 'count(*)', "where (email='$email' or otp='$otp') order by waktu desc limit 1");

if ($result > 0) {
	$datax['code'] = 200;
	$datax['msg'] = 'Berhasil';
} else {
	$datax['code'] = 404;
	$datax['msg'] = 'OTP Gagal';
}

echo encryptData($datax);
