<?php
include "../token/cek-no-token.php";

// email

if (empty($email)) {
    $datax['code'] = 404;
    $datax['msg'] = 'Not Found';
    echo encryptData($datax);
    die();
} else {
    $cekEmail = baca_tabel('user', 'count(*)', "where email='$email'");
    if ($cekEmail <= 0) {
        $datax['code'] = 404;
        $datax['msg'] = 'Email Tidak Terdaftar';
        echo encryptData($datax);
        die();
    }
}

$id_otp = generateID(100, 'otp', 'id');
$otp = randomOtp();
$sendEmailName = baca_tabel('otp', 'send_email_name', "where email='$email' order by waktu desc limit 1");
$idSendEmail = baca_tabel('send_email', 'id', "where binary send_email_name = '$sendEmailName'");
$durasi = baca_tabel('otp', 'durasi', "where email='$email' order by waktu desc limit 1");

$dataOtp['id'] = $id_otp;
$dataOtp['send_email_name'] = $sendEmailName;
$dataOtp['email'] = $email;
$dataOtp['otp'] = $otp;
$dataOtp['durasi'] = $durasi;
$dataOtp['waktu'] = date("Y-m-d H:i:s");

$result = delete_tabel('otp', "where email='$email'");
if ($result) $result = insert_tabel('otp', $dataOtp);

$params['nama_pengguna'] = baca_tabel('user', 'nama', "where email='$email' order by waktu desc limit 1");
$params['otp'] = $otp;

if ($result) {
    $datax['code'] = 200;
    $datax['msg'] = 'Kode OTP telah dikirimkan';
} else {
    $datax['code'] = 500;
    $datax['msg'] = 'Terjadi Kesalahan, Silahkan Resend Code Kembali';
}

echo encryptData($datax);

if ($datax['code'] == 200) send_email($sendEmailName, $email, $params);
