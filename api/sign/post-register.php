<?php

include "../token/cek-no-token.php";

// username, nama, email, password, no_hp, foto

if (empty($username)) {
    $datax['code'] = 500;
    $datax['msg'] = "Username Harus Diisi";
    echo encryptData($datax);
    die();
}
if (empty($email)) {
    $datax['code'] = 500;
    $datax['msg'] = "Email Harus Diisi";
    echo encryptData($datax);
    die();
}
if (empty($no_hp)) {
    $datax['code'] = 500;
    $datax['msg'] = "No HP Harus Diisi";
    echo encryptData($datax);
    die();
}
if (empty($password)) {
    $datax['code'] = 500;
    $datax['msg'] = "Password Harus Diisi";
    echo encryptData($datax);
    die();
}

$username = strtolower($username);
$email = strtolower($email);

$cekSebelummasukusername = baca_tabel("user", "count(username)", "  where username='$username'");

if ($cekSebelummasukusername > 0) {
    $datax['code'] = 500;
    $datax['msg'] = "Username Sudah Terdaftar";
    echo encryptData($datax);
    die();
}

$cekSebelummasukemail = baca_tabel("user", "count(email)", "  where email='$email'");

if ($cekSebelummasukemail > 0) {
    $datax['code'] = 500;
    $datax['msg'] = "Email Sudah Terdaftar";
    echo encryptData($datax);
    die();
}

$cekSebelummasukhp = baca_tabel("user", "count(no_hp)", "  where no_hp='$no_hp'");

if ($cekSebelummasukhp > 0) {
    $datax['code'] = 500;
    $datax['msg'] = "No Hp Sudah Terdaftar";
    echo encryptData($datax);
    die();
}

if (strpos($email, '@metlance.com')) {
    $id_roles = 'g0AlnJdRNA6yWHO';
} else if (strpos($email, '@gmail.com')) {
    $id_roles = 'ZRstYIxhG9ZzNmX';
} else {
    $id_roles = 'U3K0IBlZ7ESvaW9';
}

$dataRegist['id'] = generateID(50, 'user', 'id');
$dataRegist['password'] = base64_encode(enkrip($password));
$dataRegist['available'] = 0;
$dataRegist['username'] = $username;
$dataRegist['nama'] = $nama;
$dataRegist['email'] = $email;
$dataRegist['no_hp'] = $no_hp;
$dataRegist['id_roles'] = $id_roles;
$dataRegist['waktu'] = date('Y-m-d H:i:s');

$otp = randomOtp();
$id_otp = generateID(100, 'otp', 'id');

$dataOtp['id'] = $id_otp;
$dataOtp['send_email_name'] = 'regist';
$dataOtp['email'] = $email;
$dataOtp['otp'] = $otp;
$dataOtp['durasi'] = 30;
$dataOtp['waktu'] = date("Y-m-d H:i:s");

$params['nama_pengguna'] = $nama;
$params['otp'] = $otp;

$result = insert_tabel("user", $dataRegist);
if ($result) $result = insert_tabel('otp', $dataOtp);

if ($result) {
    $datax['code'] = 200;
    $datax['msg'] = $email;
} else {
    $datax['code'] = 500;
    $datax['msg'] = "Maaf, Pendaftaran Gagal diproses";
}

echo encryptData($datax);

if ($datax['code'] == 200) send_email('regist', $email, $params);
