<?php
include "../token/cek-token.php";

// tabs, kode, is_like

$cekTabs = baca_tabel('tabs', 'count(*)', "where nama = '$tabs'");

if (!$tabs || $tabs == '' || $cekTabs == 0) {
	$datax['code'] = 404;
	$datax['msg'] = "Tab Tidak Terdaftar";
	echo encryptData($datax);
	die();
}

$id_tabs = baca_tabel('tabs', 'id', "where nama = '$tabs'");
$id = generateID(200, 'likes', 'id');

if ($is_like == true) {
	$dataLike['is_like'] = 1;
} else {
	$dataLike['is_like'] = 0;
}

$dataLike['waktu'] = date("Y-m-d H:i:s");

$idLike = baca_tabel('likes', 'id', "where binary id_user='$id_user' and binary id_tabs='$id_tabs' and binary kode='$kode'");

if (!empty($idLike)) {
	$result = update_tabel('likes', $dataLike, "where binary id='$idLike'");
} else {
	$dataLike['id'] = $id;
	$dataLike['id_user'] = $id_user;
	$dataLike['id_tabs'] = $id_tabs;
	$dataLike['kode'] = $kode;

	$result = insert_tabel('likes', $dataLike);
}

if ($dataLike['is_like'] != 0) {
	$isLike = true;
} else {
	$isLike = false;
}

if ($result) {
	$datax['code'] = 200;
	$datax['isLike'] = $isLike;
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Tidak ada data ditemukan";
}

echo encryptData($datax);
