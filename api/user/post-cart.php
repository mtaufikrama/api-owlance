<?php
include "../token/cek-token.php";

// id, tabs, kode, total

if (!empty($id)) {
	$cekID = baca_tabel('cart', 'count(*)', "where binary id = '$id'");
	if ($cekID <= 0) {
		$datax['code'] = 404;
		$datax['msg'] = "ID tidak ada";
		echo encryptData($datax);
		die();
	}
}

$cekTabs = baca_tabel('tabs', 'count(*)', "where nama = '$tabs'");

if (empty($tabs) || $cekTabs == 0) {
	$datax['code'] = 404;
	$datax['msg'] = "Tab Tidak Terdaftar";
	echo encryptData($datax);
	die();
}

$id_tabs = baca_tabel('tabs', 'id', "where nama = '$tabs'");
$id_cart = generateID(100, 'cart', 'id');

$dataCart['total'] = $total;
$dataCart['waktu'] = date("Y-m-d H:i:s");

if (empty($id)) {
	$dataCart['id_user'] = $id_user;
	$dataCart['id_tabs'] = $id_tabs;
	$dataCart['kode'] = $kode;
	$result = insert_tabel('cart', $dataCart);
} else {
	$dataCart['id'] = $id_cart;
	$result = update_tabel('cart', $dataCart, "where binary id='$id'");
}

if ($result) {
	$datax['code'] = 200;
	$datax['msg'] = 'Berhasil Mengupload Gig';
	$datax['tabs'] = 'gig';
	$datax['kode'] = $id_cart;
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Gagal Mengupload Gig";
}

echo encryptData($datax);
