<?php
include "../token/cek-no-token.php";

// tabs, id, offset, limit
// $db->debug = true;

if (empty($limit)) $limit = 10;
if (empty($offset)) $offset = 0;

$cek = baca_tabel('tabs', 'count(*)', "where nama='$tabs'");

if ($cek <= 0) {
	$datax['code'] = 404;
	$datax['msg'] = "Tidak ada data ditemukan";
	echo encryptData($datax);
	die();
}

$id_tabs = baca_tabel('tabs', 'id', "where nama='$tabs'");

$sql = "SELECT id, id_user, comment, rate, waktu
	FROM comment 
	where binary id = id_comment 
	and binary id_tabs='$id_tabs' 
	and binary kode='$id' order by waktu desc limit $limit offset $offset ";

$run = $db->Execute($sql);

while ($get = $run->fetchRow()) {
	$getId = $get['id'];

	$sqlcomment = "SELECT id, id_user, comment, rate, waktu 
		FROM comment 
		where binary id_comment = '$getId' 
		and binary id <> id_comment 
		order by waktu desc";

	$runcomment = $db->Execute($sqlcomment);

	while ($getcomment = $runcomment->fetchRow()) {
		$getcomment['user'] = get_user($getcomment['id_user']);
		$getcomment['waktu'] = date2pesan($getcomment['waktu']);
		unset($getcomment['id_user']);
		$balasan[] = $getcomment;
	}

	$get['waktu'] = date2pesan($get['waktu']);
	$get['user'] = get_user($get['id_user']);
	unset($get['id_user']);
	$data['comment'] = $get;
	$data['balasan'] = $balasan;
	unset($balasan);
	$comment[] = $data;
}

if (is_array($comment)) {
	$datax['code'] = 200;
	$datax['msg'] = 'Berhasil';
	$datax['comments'] = $comment;
} else {
	$datax['code'] = 500;
	$datax['msg'] = "Data Tidak Ditemukan";
}
echo encryptData($datax);
