<?php
include "../token/cek-token.php";

// username, is_follow

if (empty($username)) {
    $datax['code'] = 404;
    $datax['msg'] = "Tidak ada Username";
    echo encryptData($datax);
    die();
}

$cekUsername = baca_tabel('user', 'count(*)', "where username='$username'");

if ($cekUsername <= 0) {
    $datax['code'] = 404;
    $datax['msg'] = "Username tidak ditemukan";
    echo encryptData($datax);
    die();
}

$id_user_lawan = baca_tabel('user', 'id', "where username='$username'");

$id = generateID(200, 'followers', 'id');

if ($is_follow == true) {
    $dataFollow['is_follow'] = 1;
} else {
    $dataFollow['is_follow'] = 0;
}

$dataFollow['waktu'] = date("Y-m-d H:i:s");

$idFollow = baca_tabel('followers', 'id', "where binary id_user='$id_user' and binary id_user_lawan='$id_user_lawan'");

if (!empty($idFollow)) {
    $result = update_tabel('followers', $dataFollow, "where binary id='$idFollow'");
} else {
    $dataFollow['id'] = $id;
    $dataFollow['id_user'] = $id_user;
    $dataFollow['id_user_lawan'] = $id_user_lawan;

    $result = insert_tabel('followers', $dataFollow);
}

if ($dataFollow['is_follow'] != 0) {
    $isFollow = true;
} else {
    $isFollow = false;
}

if ($result) {
    $datax['code'] = 200;
    $datax['isFollow'] = $isFollow;
} else {
    $datax['code'] = 500;
    $datax['msg'] = "Tidak ada data ditemukan";
}

echo encryptData($datax);
