<?php

include "../token/cek-token.php";

// id, foto

$foto = $_FILES['foto'];

if (empty($foto)) {
	$datax['code'] = 500;
	$datax['msg'] = 'Tidak ada foto yang diinput';
	echo encryptData($datax);
	die();
}

$size = $foto['size'];

if ($size >= 2000000) {
	$datax['code'] = 500;
	$datax['msg'] = 'Data Terlalu Besar';
	echo encryptData($datax);
	die();
}

$dataFotoChat['mime_type'] = $foto['type'];
$dataFotoChat['image'] = base64_encode(file_get_contents($foto['tmp_name']));
$dataFotoChat['waktu'] = date("Y-m-d H:i:s");

$result = update_tabel('live_chat_detail', $dataFotoChat, "where binary id='$id'");

if ($result) {
	$datax['code'] = 200;
	$datax['msg'] = 'Foto Berhasil diupload';
} else {
	$datax['code'] = 500;
	$datax['msg'] = 'Foto Gagal diupload';
}

echo encryptData($datax);
