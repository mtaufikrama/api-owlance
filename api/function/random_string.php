<?php

if (!function_exists("randomString")) {
    function randomString($length = 10, $isRandom = true, $onlyNumber = false)
    {
        if ($isRandom) $length = rand(5, $length);
        if ($onlyNumber) $characters = '0123456789';
        else $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        $charLength = strlen($characters);

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charLength - 1)];
        }

        return $randomString;
    }
}

if (!function_exists("randomOtp")) {
    function randomOtp()
    {
        $randomString = randomString(6, false, true);

        $ulangi = 0;

        while ($ulangi == 0) {
            $cek = baca_tabel('otp', 'otp', "where binary otp = '$randomString'");
            if (!empty($cek)) {
                $randomString = randomString(6, false, true);
            } else {
                $ulangi = 1;
            }
        }

        return $randomString;
    }
}

if (!function_exists("generateID")) {
    function generateID($length = 10, $tbl = '', $fld = '')
    {
        $ulangi = 0;
        $generateString = randomString($length);
        while ($ulangi == 0) {
            $cek = baca_tabel($tbl, $fld, "where binary $fld = '$generateString'");
            if ($cek) {
                $generateString = randomString($length);
            } else {
                $ulangi = 1;
            }
        }
        return $generateString;
    }
}
