<?php
if (!function_exists("image_link")) {
    function image_link($id = '')
    {
        if (empty($id)) {
            $link = null;
        } else {
            $link = "https://api.metlance.com/api/v1/get-foto.php?id=$id";
        }
        return $link;
    }
}
